
// Bài 1

function tinhLuong(idLuongNgay, idNgayLam, idClick, idResult){
    let luongNgay = document.querySelector(idLuongNgay);
    let ngayLam = document.querySelector(idNgayLam);
    let click = document.querySelector(idClick);
    let result = document.querySelector(idResult);

    if(luongNgay && ngayLam && click && result){
        click.addEventListener("click",function(){
            var tinhTienLuong = parseInt(luongNgay.value) * parseInt( ngayLam.value)
            result.innerHTML = `<span>Tiền lương của bạn là: ${tinhTienLuong}VND</span>`
        })
    }
    
}

tinhLuong(".luongMotNgay", ".ngayCong", ".button-click" , ".result")


//Bài 2

function tinhTrungBinh(idNumber1, idNumber2, idNumber3, idNumber4, idNumber5, idClick, idResult){
    let Number1 = document.querySelector(idNumber1);
    let Number2 = document.querySelector(idNumber2);
    let Number3 = document.querySelector(idNumber3);
    let Number4 = document.querySelector(idNumber4);
    let Number5 = document.querySelector(idNumber5);
    let Click  = document.querySelector(idClick);
    let Result = document.querySelector(idResult);

    if(Number1 && Number2 && Number3 && Number4 && Number5 && Click && Result){
        Click.addEventListener("click",function(){
            let giaTriTrungBinh= (parseInt(Number1.value) + parseInt(Number2.value) + parseInt(Number3.value) + parseInt(Number4.value) + parseInt(Number5.value))/5;
            Result.innerHTML =  `<span>Giá trị trung bình là: ${giaTriTrungBinh}</span>`
        })
    }
}

tinhTrungBinh(".number1" , ".number2" , ".number3" , ".number4" , ".number5",".medium",".resultMedium")

//bài 3

function doiTien(idMoney, idClickChange, idChange){
    let Money = document.querySelector(idMoney);
    let Clickchange = document.querySelector(idClickChange);
    let Change = document.querySelector(idChange);

    if(Money && Clickchange && Change){
        Clickchange.addEventListener("click",function(){
            let resultChange = parseInt(Money.value)* 23500;
            Change.innerHTML= `<span>Thành tiền: ${resultChange} VNĐ</span>`
        })
    }
}

doiTien(".dola" , ".click-change" , ".change")



function hinhChuNhat(idLongs, idWidth, idCount, idResultLongs, idResultWidth){
    let longs = document.querySelector(idLongs);
    let width = document.querySelector(idWidth);
    let count = document.querySelector(idCount);
    let resultLongs = document.querySelector(idResultLongs);
    let resultWidth = document.querySelector(idResultWidth);


    if(longs && width && count && resultLongs && resultWidth){
        count.addEventListener("click",function(){
            let area = parseInt(longs.value) * parseInt(width.value);
            let perimeter = (parseInt(longs.value) + parseInt(width.value)) *2;
            resultLongs.innerHTML = `<span>Diện tích hình chữ nhật là: ${area}</span>`
            resultWidth.innerHTML = `<span>Chu vi hình chữ nhật là: ${perimeter}</span>`
        })
    }
}

hinhChuNhat(".longs", ".width", ".count" , ".resultLongs", ".resultWidth");


function tinhTong(idNumbers, idTotal, idResultTotal){
    let numbers = document.querySelector(idNumbers);
    let total = document.querySelector(idTotal);
    let resultTotial = document.querySelector(idResultTotal);

    if (numbers && total && resultTotial){
        total.addEventListener("click", function(){
            let numberEl = parseInt(numbers.value)
            let unit = numberEl % 10;
            let dozen = Math.floor(numberEl / 10); 
            let resultNumber = unit + dozen;
            resultTotial.innerHTML =  `<span>Tổng 2 ký số là: ${resultNumber}</span>`
        })
    }
}

tinhTong(".numbers", ".total", ".total-results")